# SOC metrics workbook

SOC metrics is an Azure Sentinel workbook that was created to provide an overview of your Analytics Rules, Created Incidents, and most importantly the related SOC metrics.

The SOC Metrics workbook is still under heavy development. Issues and bugs are definitely part of this product at this point.

The .workbook file contains the code that you can copy into your Sentinel, or you can use the .json file with is an ARM template.

For more details of how to use the workbook please install the workbook itself and check the "Manual" tab. **Please be aware, to fully utilize the workbook you (and your analyst) have to follow the workflow specified on the Manual tab of the workbook.** Otherwise some metrics are not going to be available. If you have a different workflow and the proper engineers then it is going to be easy to modify the workbook.


## Examples graphs:
#### Average and Percentile -based metrics:
![Average and Percentile -based metrics img](IMG/metrics.PNG "Average and Percentile -based metrics")

#### False Positive rules:
![False Positive rules img](IMG/most_fp_heavy_percentage.PNG "False Positive rules")
